#!/bin/bash

# when using CANable (via USB)
# slcand -o -c -s8 /dev/serial/by-id/usb-CANtact* can0
# sleep 3
# ip link set up can0

# when using PICAN2 (SPI)
LINEONE='dtoverlay=mcp2515-can0,oscillator=16000000,interrupt=25'
LINETWO='dtoverlay=spi-bcm2835'
FILE=/mnt/boot/config.txt
grep -qF "$LINEONE" "$FILE" || (echo "$LINEONE" >> "$FILE" && echo "$LINETWO" >> "$FILE")

# bring up the PICAN2
ip link set can0 up type can bitrate 250000

cd /usr/src/app

/usr/bin/wget -q --spider http://google.com

if [ $? -eq 0 ]; then
  echo "Online. Installing npm packages..."
  npm install
else
  echo "Offline. Skipping npm install."
fi

npm start
